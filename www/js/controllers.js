

var app=angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope,$location, $ionicModal, $timeout,$rootScope) {
  // Form data for the login modal
  
   
   $scope.username = 'hello';
   $scope.password = 'hello';

  // Create the login modal that we will use later
  // $ionicModal.fromTemplateUrl('templates/login.html', {
    // scope: $scope
  // }).then(function(modal) {
    // $scope.modal = modal;
  // });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    // $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    // $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    if($scope.username==='hello' && $scope.password==='hello'){
  //$rootScope.loggedUser = $scope.username;
  $location.path( "/app/Home" );
  //$scope.$apply(function() { $location.path("/playlists"); });
  }
  else{
  $location.path( "/app/login" );
  }

    // Simulate a login delay. Remove this and replace with your login
    // // code if using a login system
    // $timeout(function() {
      
    // }, 1000);
  }
})

.controller('AccountSummaryCtrl', function($scope,$location,$http) {  
   $scope.typeofAccount=[{id:1,name:"Savings Account"},{id:2,name:"Checking Account"}]

    $scope.changedValueBalance=function(item){          
            
  if(item.name==="Savings Account"){
  $location.path( "/app/SavingAccountSummary" );
  }
  else{
  $location.path( "/app/CheckAccountSummary" );
  }
  }
       
})
.controller('HomeCtrl', function($scope, $stateParams) {
})


.controller('CheckAccountSummaryCtrl', function($scope, $stateParams) {
$scope.values = [
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday              $2000', id: 1  },
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1 }
  ];
  
})
.controller('SavingAccountSummaryCtrl', function($scope, $stateParams) {
$scope.values1 = [
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday              $2000', id: 1  },
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1 }
  ];
  
})
.controller('CheckAccountSummaryCtrl', function($scope, $stateParams) {
$scope.values = [
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday            $2000', id: 1  },
    { title: 'Monday              $2000', id: 1  },
    { title: 'Monday            $2000', id: 1 },
    { title: 'Monday            $2000', id: 1 }
  ];
  
})
.controller('AccountBalanceCtrl', function($scope,$location,$http, $ionicModal) {

   
    $scope.typeofAccount=[{id:1,name:"Savings Account"},{id:2,name:"Checking Account"}]

    $scope.changedValue=function(item){
  if(item.name==="Savings Account"){
  $location.path( "/app/SavingsBalanceAccount" );
  }
  else if(item.name==="Checking Account"){
  $location.path( "/app/CheckBalanceAccount" );
  }
  else{
  }
    }   

 
})

.controller('CheckBalanceAccountCtrl', function($scope, $stateParams) {
})
.controller('SavingsBalanceAccountCtrl', function($scope, $stateParams) {
})
.controller('FundTransferCtrl', function($scope,$location, $stateParams) {
$scope.typeofAccountFromFundTransfer=[{id:1,name:"Savings Account"},{id:2,name:"Checking Account"}];
$scope.typeofAccountToFundTransfer=[{id:1,name:"Savings Account"},{id:2,name:"Checking Account"},{id:2,name:"Third Party"}];
$scope.changedValueFromFundTransfer=function(item){
$scope.blisterPackTemplateSelectedFromFundTransfer=item.name;
}

$scope.changedValueToFundTransfer=function(item){
$scope.blisterPackTemplateSelectedToFundTransfer=item.name;
}
    $scope.clickForFundTransfer=function(item){
  if($scope.blisterPackTemplateSelectedToFundTransfer==="Savings Account" && $scope.blisterPackTemplateSelectedFromFundTransfer==="Checking Account" ){
  $location.path( "/app/CheckingToSavingsFundTransfer" );
  }
  else if($scope.blisterPackTemplateSelectedToFundTransfer==="Third Party" && $scope.blisterPackTemplateSelectedFromFundTransfer==="Checking Account" ){
  $location.path("app/FundTransferCheckingToThirdParty");
  }
  else if($scope.blisterPackTemplateSelectedToFundTransfer==="Third Party" && $scope.blisterPackTemplateSelectedFromFundTransfer==="Savings Account" ){
  $location.path("app/FundTransferSavingsToThirdParty");
  }
  else if($scope.blisterPackTemplateSelectedToFundTransfer==="Checking Account" && $scope.blisterPackTemplateSelectedFromFundTransfer==="Savings Account" ){
  $location.path("app/SavingToCheckingFundTransfer");
  }
  else{
  }
    }   
})

.controller('CheckingToSavingsFundTransferCtrl', function($scope,$location, $stateParams) {
$scope.TransferCheckingToSavings=function(){
$location.path( "/app/FundTransfer" );
}
})

.controller('SavingToCheckingFundTransferCtrl', function($scope,$location, $stateParams) {

$scope.TransferSavingsToTransfer=function(){
$location.path( "/app/FundTransfer" );
}
})

.controller('FundTransferCheckingToThirdPartyCtrl', function($scope,$location, $stateParams) {
$scope.TransferToThirdParty=function(){
$location.path( "/app/FundTransfer" );
}
})

.controller('VoiceInputCtrl', function($scope,$location, $stateParams,$http) {  
  $scope.voiceInputVisible = function(){

   if($location.path() === '/app/login'){
    return false;
   }
   else{
    return true;
   }
 }
        $scope.initiateVoiceRecognition=function(){
                var maxMatches = 1;
                var promptString = "Speak now"; // optional
                var language = "en-US";                     // optional
                window.plugins.speechrecognizer.startRecognize(function(result){  
      
        alert(result); 
       
        var responsePromise = $http({url: "http://parserservice.apphb.com/api/values/", method: "GET",params: {id: result}});   
          
                responsePromise.success(function(data, status, headers, config) { 
              
         if(data.Success===true){     
       
                 $location.path( "/app/"+ data.ControllerName );
                 }
         else{
         alert(data.Input +" not identified");
          

         }
         });
         
                responsePromise.error(function(data, status, headers, config) {
                 alert(headers,data);
                });
                }, function(errorMessage){
                    console.log("Error message: " + errorMessage);
                }, maxMatches, promptString, language); 
}       
  

})

.controller('UtilityBillsCtrl', function($scope,$location, $stateParams) {
$scope.CreditBill=function(){
$location.path( "/app/CreditBill");
}
$scope.TelephoneBill=function(){
$location.path( "/app/TelephoneBill");
}
$scope.ElectricityBill=function(){
$location.path( "/app/ElectricityBill");
}
$scope.InternetBill=function(){
$location.path( "/app/InternetBill");
}
})


.controller('FixedCtrl', function($scope,$location, $stateParams) {
})

.controller('DepositChequeCtrl', function($scope,$location, $stateParams) {
  
})

.controller('RecurringCtrl', function($scope,$location, $stateParams) {
})

.controller('NearestAtmCtrl', function($scope, $stateParams) {
})
.controller('PayBillCtrl', function($scope, $stateParams) {
})
.controller('TelephoneBillCtrl', function($scope, $stateParams) {
})
.controller('CreditBillCtrl', function($scope, $stateParams) {
})
.controller('InternetBillCtrl', function($scope, $stateParams) {
})
.controller('ElectricityBillCtrl', function($scope, $stateParams) {
})

.controller('FundTransferSavingsToThirdPartyCtrl', function($scope,$location, $stateParams) {
$scope.TransferToThirdParty=function(){
$location.path( "/app/FundTransfer" );
}
});

